document.querySelector('#boton').addEventListener('click', mostrarelemento);

function mostrarelemento(){
    const xhttp = new XMLHttpRequest();

    xhttp.open('GET', 'datos.json', true);
    xhttp.send();
    xhttp.onreadystatechange = function(){

        if(this.readyState == 4 && this.status == 200){
            let elemento = JSON.parse(this.responseText);
            let resultado = document.querySelector('#datos');
            resultado.innerHTML = '';
            for(let item of elemento){
                resultado.innerHTML += `
                <tr>
                    <td>${item.id}</td>
                    <td>${item.Cedula}</td>
                    <td>${item.Nombre}</td>
                    <td>${item.Correo}</td>
                    <td>${item.Direccion}</td>
                    <td>${item.Telefono}</td>
                    <td>${item.Curso}</td>
                    <td>${item.Paralelo}</td>
                </tr>
                `
            }
        }
    }
}

